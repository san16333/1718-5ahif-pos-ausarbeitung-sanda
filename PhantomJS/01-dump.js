//Variable Declarations
const phantom = require('phantom');

(async function() {
    //Create Phantom Instance
    const instance = await phantom.create();
    //Create Page Instance
    const page = await instance.createPage();
    //Log Requests
    await page.on("onResourceRequested", function(requestData) {
        console.info('Requesting', requestData.url)
    });

    //Print Status and open to wikipedia page
    const status = await page.open('https://en.wikipedia.org/wiki/PhantomJS');
    console.log(status);

    //Print Out Page Content
    const content = await page.property('content');
    console.log(content);

    //Close Phantom Instance
    await instance.exit();
}());

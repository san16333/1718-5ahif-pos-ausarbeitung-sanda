//Variable Declarations
const phantom = require('phantom');

(async function() {
    //Create Phantom Instance
    const instance = await phantom.create();
    //Create Page Instance
    const page = await instance.createPage();

    //Print Status and open to wikipedia page
    const status = await page.open('https://en.wikipedia.org/wiki/PhantomJS');
    console.log(status);

    //Get Links
    page.evaluate(function(){
      return [].map.call(document.querySelectorAll('#content a[href*=wiki]'), function(link){
      	return link.href;
      });
    }).then(function(links){
      console.log(links);
    });

/*
    //Set ViewportSize
    page.property('viewportSize', {
      width: 1024,
      height: 768
    });

    //Clip "Screen"
    page.property('clipRect', {
      top: 0,
      left: 0,
      width: 1024,
      height: 768
    });

    //Make Screenshot
    page.render('./test.jpg');
*/

    //Close Phantom Instance
    await instance.exit();
}());

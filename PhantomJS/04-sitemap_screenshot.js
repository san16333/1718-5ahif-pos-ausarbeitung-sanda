//Variable Declarations
const phantom = require('phantom');

(async function() {
  //Create Phantom Instance
  const instance = await phantom.create();
  //Create Page Instance
  const page = await instance.createPage();

  //Print Status and open to wikipedia page
  const status = await page.open('https://en.wikipedia.org/wiki/PhantomJS');
  console.log(status);


  var links = await page.evaluate(function(){
    return [].map.call(document.querySelectorAll('.mw-parser-output p')[0].querySelectorAll("a"), function(link){
    	return link.href;
    });
  });


  console.log(links.length);

  const new_page = await instance.createPage();

  new_page.property('viewportSize', {
    width: 1024,
    height: 768
  });

  new_page.property('clipRect', {
    top: 0,
    left: 0,
    width: 1024,
    height: 768
  });

  console.time('saving');
  for(var i=0; i<links.length; i++) {
    console.log(links[i]);
    await new_page.open(links[i]);
    await new_page.render('./sitemap_screenshots/' + i + '.jpg');
  }
  console.timeEnd('saving');

  instance.exit();
}());

const puppeteer = require('puppeteer');

(async() => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://en.wikipedia.org/wiki/Puppeteer');

  var links = await page.evaluate(function(){
    return [].map.call(document.querySelectorAll('.thumb.tright ~ p a'), function(link){
      return link.href;
    });
  });

  console.log(links.length);
  const new_page = await browser.newPage();
  await new_page.setViewport({width: 1024, height: 768});

  console.time('saving');
  for(var i=0; i<links.length; i++) {
    console.log(links[i]);
    await new_page.goto(links[i]);
    await new_page.screenshot({path: './sitemap_screenshots/'+i+'.jpg'});
  }
  console.timeEnd('saving');

  await browser.close();
})();

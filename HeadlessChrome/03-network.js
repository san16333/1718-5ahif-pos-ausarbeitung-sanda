const puppeteer = require('puppeteer');

(async() => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://en.wikipedia.org/wiki/Puppeteer');

  var links = await page.evaluate(function(){
    return [].map.call(document.querySelectorAll('.thumb.tright ~ p a'), function(link){
      return link.href;
    });
  });

  console.log(links.length);
  const new_page = await browser.newPage();
  await new_page.setViewport({width: 1024, height: 768});

  //Connect To DevTools
  const client = await new_page.target().createCDPSession();
  //1000Kb/s connection + 20ms latency
  await client.send('Network.emulateNetworkConditions', {
    'offline': false,
    'downloadThroughput': 1000 * 1024 / 8,
    'uploadThroughput': 1000 * 1024 / 8,
    'latency': 20
  });

  console.time('saving');
  for(var i=0; i<links.length; i++) {
    console.log(links[i]);
    await new_page.goto(links[i]);
    await new_page.screenshot({path: './sitemap_screenshots/'+i+'.jpg'});
  }
  console.timeEnd('saving');

  await browser.close();
})();

const puppeteer = require('puppeteer');

(async() => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://en.wikipedia.org/wiki/Puppeteer');
  await page.setViewport({width: 1920, height: 1080});
  await page.screenshot({path: '01.jpg'});

  await browser.close();
})();

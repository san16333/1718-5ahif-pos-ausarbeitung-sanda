//Variable Declarations
const Browser = require('zombie');

const browser = new Browser();

browser.visit('https://en.wikipedia.org/wiki/Headless_browser', function(){
  console.log(browser.query('#firstHeading').innerHTML);
  console.log(browser.url);
  //console.log(browser);
  browser.fill('search', 'zombie').pressButton('go', function(){
    console.log(browser.url);
  });
});

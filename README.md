# POS-Ausarbeitung
von Sanda Luca

## Allgemeines
Die Dokumentation für dieses Repo ist in der Datei *Dokumentation.pdf* vorhanden

## Installation
Um die einzelnen Test durchzuführen wird Node.js benötigt.

Innerhalb der drei Varianten-Ordner müssen noch mittels `npm install` die notwendigen Module installiert werden.

Dannach sollten die einzelnen Tests ganz einfach mittels `node [NAME-DES-TEST]` ausführbar sein.
